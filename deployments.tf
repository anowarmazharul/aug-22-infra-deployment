resource "kubernetes_deployment_v1" "backend" {
  metadata {
    name = "backend"
    labels = {
      test = "backend"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "backend"
      }
    }

    template {
      metadata {
        labels = {
          test = "backend"
        }
      }

      spec {
        container {
          image = "farhaz1449/backend-opt"
          name  = "backend"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "backend_svc" {
  metadata {
    name = "backend"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.backend.metadata.0.labels.test
    }
    
    port {
      port        = 5000
      target_port = 5000
    }

    type = "NodePort"
  }
}



#frontend deployment and seervice

resource "kubernetes_deployment_v1" "frontend" {
  metadata {
    name = "frontend"
    labels = {
      test = "frontend"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "frontend"
      }
    }

    template {
      metadata {
        labels = {
          test = "frontend"
        }
      }

      spec {
        container {
          image = "mazhar23/fe-frontend:a09c7139"
          name  = "frontend"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "frontend_svc" {
  metadata {
    name = "frontend"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.frontend.metadata.0.labels.test
    }
    
    port {
      port        = 80
      target_port = 80
    }

    type = "LoadBalancer"
  }
}
